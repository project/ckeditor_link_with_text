(function ($, Drupal, drupalSettings, CKEDITOR) {

  CKEDITOR.plugins.add('link_with_text', {
    icons: 'link_with_text',
    hidpi: true,

    init: function init(editor) {
      editor.addCommand('link_with_text', {
        allowedContent: {
          a: {
            attributes: {
              '!href': true,
              '!title': true,
              '!target': true,
              '!rel': true,
            },
            classes: {}
          }
        },
        requiredContent: new CKEDITOR.style({
          element: 'a',
          attributes: {
            href: ''
          }
        }),
        modes: {wysiwyg: 1},
        canUndo: true,
        exec: function exec(editor) {
          var linkElement = getSelectedLink(editor);
          var linkDOMElement = null;

          var existingValues = {};
          if (linkElement && linkElement.$) {
            linkDOMElement = linkElement.$;

            var attribute = null;
            var attributeName;
            for (var attrIndex = 0; attrIndex < linkDOMElement.attributes.length; attrIndex++) {
              attribute = linkDOMElement.attributes.item(attrIndex);
              attributeName = attribute.nodeName.toLowerCase();

              if (attributeName.substring(0, 15) === 'data-cke-saved-') {
                continue;
              }

              existingValues[attributeName] = linkElement.data('cke-saved-' + attributeName) || attribute.nodeValue;
            }
          }

          var saveCallback = function saveCallback(returnValues) {
            editor.fire('saveSnapshot');

            if (!linkElement) {
              var selection = editor.getSelection();
              var range = selection.getRanges(1)[0];

              var style = new CKEDITOR.style({
                element: 'a',
                attributes: returnValues.attributes
              });
              style.type = CKEDITOR.STYLE_INLINE;
              style.applyToRange(range);
              range.select();

              editor.insertText(returnValues.link_text);

            } else if (linkElement) {
              Object.keys(returnValues.attributes || {}).forEach(function (attrName) {
                console.log(attrName);
                if (returnValues.attributes[attrName].length > 0) {
                  var value = returnValues.attributes[attrName];
                  linkElement.data('cke-saved-' + attrName, value);
                  linkElement.setAttribute(attrName, value);
                } else {
                  linkElement.removeAttribute(attrName);
                }
              });
              editor.insertText(returnValues.link_text);
            }
            editor.fire('saveSnapshot');
          };

          var dialogSettings = {
            title: linkElement ? editor.config.drupalLink_dialogTitleEdit : editor.config.drupalLink_dialogTitleAdd,
            dialogClass: 'editor-linkwithtext-dialog'
          };

          Drupal.ckeditor.openDialog(editor, Drupal.url('ckeditor_link_with_text/dialog/file/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);
        }
      });

      editor.setKeystroke(CKEDITOR.CTRL + 77, 'link_with_text');

      if (editor.ui.addButton) {
        editor.ui.addButton('internalLinkButton', {
          label: Drupal.t('Link with Text'),
          command: 'link_with_text',
          icon: this.path + '/file.png'
        });
      }

      if (editor.addMenuItems) {
        editor.addMenuItems({
          link: {
            label: Drupal.t('Edit Link'),
            command: 'link_with_text',
            group: 'link',
            order: 1
          },
        });
      }

      if (editor.contextMenu) {
        editor.contextMenu.addListener(function (element, selection) {
          if (!element || element.isReadOnly()) {
            return null;
          }
          var anchor = getSelectedLink(editor);
          if (!anchor) {
            return null;
          }

          var menu = {};
          if (anchor.getAttribute('href') && anchor.getChildCount()) {
            menu = {
              link_with_text: CKEDITOR.TRISTATE_OFF,
            };
          }
          return menu;
        });
      }
    }
  });

  function getSelectedLink(editor) {
    var selection = editor.getSelection();
    var selectedElement = selection.getSelectedElement();

    if (selectedElement && selectedElement.is('a')) {
      return selectedElement;
    }

    var range = selection.getRanges(true)[0];

    if (range) {
      range.shrink(CKEDITOR.SHRINK_TEXT);
      return editor.elementPath(range.getCommonAncestor()).contains('a', 1);
    }
    return null;
  }

})(jQuery, Drupal, drupalSettings, CKEDITOR);

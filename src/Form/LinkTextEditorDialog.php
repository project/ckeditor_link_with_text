<?php

namespace Drupal\ckeditor_link_with_text\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\filter\Entity\FilterFormat;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a link dialog for text editors.
 */
class LinkTextEditorDialog extends FormBase {

    /**
     * The file storage service.
     *
     * @var \Drupal\Core\Entity\EntityStorageInterface
     */
    protected $editor_storage;

    /**
     * The entity repository service.
     *
     * @var \Drupal\Core\Entity\EntityRepositoryInterface
     */
    protected $entityRepository;

    /**
     * Constructs a form object for image dialog.
     *
     * @param \Drupal\Core\Entity\EntityStorageInterface    $file_storage
     *   The file storage service.
     * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
     *   The entity repository service.
     */
    public function __construct(EntityStorageInterface $editor_storage, EntityRepositoryInterface $entity_repository) {
        $this->editor_storage = $editor_storage;
        $this->entityRepository = $entity_repository;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
                $container->get('entity_type.manager')->getStorage('editor'), $container->get('entity.repository')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'ckeditor_internal_link_dialog';
    }

    /**
     * {@inheritdoc}
     *
     * @param array                                $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     * @param \Drupal\filter\Entity\FilterFormat   $filter_format
     *   The filter format for which this dialog corresponds.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = null) {
        $form['#tree'] = true;
        $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
        $form['#prefix'] = '<div id="editor-link-with-text-dialog-form">';
        $form['#suffix'] = '</div>';
        $form['link_text'] = [
            '#title' => $this->t('Link Text'),
            '#type' => 'textfield',
            '#maxlength' => 2048,
            '#required' => true,
        ];
        $form['attributes']['rel'] = [
            '#type' => 'hidden',
        ];
        $form['attributes']['href'] = [
            '#title' => $this->t('URL'),
            '#type' => 'textfield',
            '#maxlength' => 2048,
            '#required' => true,
            '#autocomplete_route_name' => 'ckeditor_internal_link.autocomplete',
        ];
        $form['attributes']['title'] = [
            '#title' => $this->t('Title'),
            '#type' => 'textfield',
            '#maxlength' => 2048,
            '#required' => true,
        ];
        $form['attributes']['target'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Open in new window'),
            '#return_value' => '_blank',
        ];
        $form['actions'] = [
            '#type' => 'actions',
        ];
        $form['actions']['save_modal'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#submit' => [],
            '#ajax' => [
                'callback' => '::submitForm',
                'event' => 'click',
            ],
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $response = new AjaxResponse();
        $target = $form_state->getValue(['attributes', 'target']);
        if ($target == '0') {
            $form_state->setValue(['attributes', 'target'], "_self");
        }
        $link_text = $form_state->getValue('link_text');
        if ($link_text) {
            $form_state->setValue(['attributes', 'rel'], $link_text);
        }
        if ($form_state->getErrors()) {
            unset($form['#prefix'], $form['#suffix']);
            $form['status_messages'] = [
                '#type' => 'status_messages',
                '#weight' => -10,
            ];
            $response->addCommand(new HtmlCommand('#editor-link-with-text-dialog-form', $form));
        } else {
            $response->addCommand(new EditorDialogSave($form_state->getValues()));
            $response->addCommand(new CloseModalDialogCommand());
        }
        return $response;
    }

}

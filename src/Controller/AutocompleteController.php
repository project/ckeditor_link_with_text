<?php

/**
 * The Autocomplete Controller.
 *
 * @file
 * AutocompleteController
 */

namespace Drupal\ckeditor_link_with_text\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Path\AliasManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for internal link autocomplete routes.
 */
class AutocompleteController extends ControllerBase
{

    /**
     * The Query Factory.
     *
     * @var \Drupal\Core\Entity\Query\QueryFactory
     */
    protected $entityQuery;

    /**
     * The Alias Manager.
     *
     * @var \Drupal\Core\Path\AliasManager
     */
    protected $aliasManager;

    /**
     * Constructs a new SiteConfiguration Form.
     *
     * @param \Drupal\Core\Entity\Query\QueryFactory $entityQuery
     *   The The Query Factory.
     * @param \Drupal\Core\Path\AliasManagerInterface $aliasManager
     *   The alias manager.
     */
    public function __construct(QueryFactory $entityQuery, AliasManager $aliasManager)
    {
        $this->entityQuery = $entityQuery;
        $this->aliasManager = $aliasManager;
    }

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     *   The Container Interface.
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('entity.query'), $container->get('path.alias_manager')
        );
    }

    /**
     * Menu callback for internal link search autocompletion.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The current request.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse $matches
     *   A JSON response containing autocomplete suggestions.
     */
    public function autocomplete(Request $request)
    {
        $matches = [];
        $string = $request->query->get('q');
        if ($string) {
            $matches = [];
            $query = $this->entityQuery->get('node')
                ->condition('status', 1)
                ->condition('title', '%' . db_like($string) . '%', 'LIKE');
            $nids = $query->execute();
            $result = entity_load_multiple('node', $nids);
            foreach ($result as $row) {
                $alias = $this->aliasManager->getAliasByPath(
                    '/node/' .$row->nid->value
                );
                $matches[] = ['value' => $alias, 'label' => $row->title->value];
            }
        }
        return new JsonResponse($matches);
    }

}

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides the hyperlink text field with the link.
When you enable this module it provides a new link field with a text interface.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install the Ckeditor Link With Text module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.

CONFIGURATION
-------------

  1. Navigate to Administration > Configuration > Content authoring > Text formats and editors and configure text format.
  2. Internal Link button in active toolbar.

MAINTAINERS
-----------

8.x-1.x Developed and maintained by:
 * Manish Jain (https://www.drupal.org/user/2336646)
